package com.example.a1430169.lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void showData(View view) {
        // get the references to the view widgets
        // no reference to the header TextView, I don't manipulate it
        EditText et = (EditText) findViewById(R.id.amountField);
        double amount = Double.parseDouble(et.getText().toString());

        et = (EditText) findViewById(R.id.termField);
        int term = Integer.parseInt(et.getText().toString());

        et = (EditText) findViewById(R.id.rateField);
        double rate = Double.parseDouble(et.getText().toString());
        LoanCalculator lc = new LoanCalculator(amount, term, rate);

        TextView tv1 = (TextView)findViewById(R.id.monthlyValue);
        String str1 = Double.toString(lc.getMonthlyPayment());
        tv1.setText(str1);

        TextView tv2 = (TextView)findViewById(R.id.totalValue);
        String str2 = Double.toString(lc.getTotalCostOfLoan());
        tv2.setText(str2);

        TextView tv3 = (TextView)findViewById(R.id.intValue);
        String str3 = Double.toString(lc.getTotalInterest());
        tv3.setText(str3);


    }

    public void clearData(View view) {
        ((EditText)findViewById(R.id.amountField)).setText("");
        ((EditText)findViewById(R.id.termField)).setText("");
        ((EditText)findViewById(R.id.rateField)).setText("");




    }

}
